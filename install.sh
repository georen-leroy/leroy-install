#!/bin/bash
bashProfile="eval $(ssh-agent) && ssh-add /home/leroy/leroy"

rootCheck() {
    if [[ $EUID -ne 0 ]]; then
       echo "This script must be run as root" 1>&2
       exit 1
    fi
}

rootfsCheck() {
    read -r -p "Have you expanded the rootfs? [y/N] " response
    response=${response,,}    # tolower
    if [[ ! $response =~ ^(yes|y)$ ]]; then
        exit 1
    fi
}

newPass() {
    echo "Enter a new password for the pi user"
    passwd pi
    echo "Enter a new password for the leroy user"
    passwd leroy
    echo "Enter a new password for the root user"
    passwd root
}

newUser() {
    if [[ ! -s ~/.bash_profile ]]; then
        echo "${bashProfile}" > ~/.bash_profile
    fi
    if [[ ! -d /home/leroy ]]; then
        echo "Creating user leroy"
        adduser leroy
        usermod -aG pi,adm,sudo,audio,video,plugdev,users,input,netdev,gpio,i2c,spi leroy
        su -c "ssh-keygen -t rsa -b 4096 -N '' -f ~/.ssh/leroy" leroy
        su -c "echo \"${bashProfile}\" > ~/.bash_profile" leroy
        su -c "source ~/.bash_profile" leroy
        su -c "ssh-add ~/.ssh/leroy" leroy
    fi
    su -c "cat ~/.ssh/leroy.pub" leroy
    echo "Add this public key to the leroy repo"
    read -n1 -r -p "Press space to continue..." key
}

installDeps() {
    #Install deps, setup users
    curl -sL https://deb.nodesource.com/setup_4.x -o /tmp/setup_4.x
    bash /tmp/setup_4.x
    apt-get -y upgrade
    apt-get install -y nodejs git erlang erlang-nox erlang-dev libmozjs185-1.0 libmozjs185-dev libcurl4-openssl-dev libicu-dev libssl-dev
    npm -g install forever
    useradd -d /var/lib/couchdb couchdb
    mkdir -p /usr/local/{lib,etc}/couchdb /usr/local/var/{lib,log,run}/couchdb /var/lib/couchdb
    chown -R couchdb:couchdb /usr/local/{lib,etc}/couchdb /usr/local/var/{lib,log,run}/couchdb
    chmod -R g+rw /usr/local/{lib,etc}/couchdb /usr/local/var/{lib,log,run}/couchdb
}

installCouch() {
    if [[ ! $(which couchdb) ]]; then
        #Install and run CouchDB
        cd ~
        curl -sL http://mirror.tcpdiag.net/apache/couchdb/source/1.6.1/apache-couchdb-1.6.1.tar.gz -o ~/apache-couchdb-1.6.1.tar.gz
        tar xf ~/apache-couchdb-1.6.1.tar.gz
        cd ~/apache-couchdb-1.6.1
        ./configure --prefix=/usr/local --with-js-lib=/usr/lib --with-js-include=/usr/include/js --enable-init
        make
        make install
        ln -s /usr/local/etc/init.d/couchdb /etc/init.d/couchdb
        chown couchdb:couchdb /usr/local/etc/couchdb/local.ini
        /etc/init.d/couchdb start
        update-rc.d couchdb defaults
        cd ~
    fi
    if [[ $(curl -I http://127.0.0.1:5984/ | head -n 1 | cut -d$' ' -f2) == 200 ]]; then
        echo "CouchDB server is running"
        curl -X PUT -d {} localhost:5984/system
        curl -X PUT -d {} localhost:5984/system/admin
        curl -X PUT -d {} localhost:5984/system/modules
    fi
}

cloneLeroy() {
    cd ~
    su -c "cd ~ && git clone git@bitbucket.org:georen-leroy/leroy.git" leroy
    su -c "cd ~/leroy && git checkout develop && npm install" leroy
}

copyFiles() {
    DIR="/home/pi/leroy-install"
    if ! grep -q bitbucket "/etc/ssh/ssh_config"; then
        mv /etc/ssh/ssh_config /etc/ssh/ssh_config.bak
        cat /etc/ssh/ssh_config.bak $DIR/ssh_config > /etc/ssh/ssh_config
    fi
    if ! grep -q bitbucket "/etc/ssh/ssh_known_hosts"; then
        mv /etc/ssh/ssh_known_hosts /etc/ssh/ssh_known_hosts.bak
        cat /etc/ssh/ssh_known_hosts.bak $DIR/ssh_known_hosts > /etc/ssh/ssh_known_hosts
    fi

    mkdir -p /var/leroy/bin/
    cp $DIR/update-modules /etc/cron.daily/
    cp $DIR/update-modules.sh /var/leroy/bin/
    chown -R root:root /var/leroy/
    chown root:root /etc/cron.daily/update-modules
    chmod -R 700 /var/leroy/
    chmod 755 /etc/cron.daily/update-modules

    mkdir /home/leroy/.ssh
    cp /home/leroy/.ssh/{leroy,leroy.pub} ~/.ssh
    chown pi:users ~/.ssh/{leroy,leroy.pub}
    chmod 600 ~/.ssh/{leroy,leroy.pub}

    cp /home/leroy/.ssh/{leroy,leroy.pub} /root/.ssh
    chown root:root ~/.ssh/{leroy,leroy.pub}
    chmod 600 ~/.ssh/{leroy,leroy.pub}
}

rootCheck
rootfsCheck
installDeps
installCouch
newUser
copyFiles
cloneLeroy
newPass
